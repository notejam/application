#!/bin/sh

set -e

if [ "${APP_DEBUG}" = 1 ]; then
    set -x
fi

echo "-> Setting env to '${APP_ENV}'"

if [ "${APP_ENV}" = "local" ] || [ "${1}" = "test" ]; then
    composer install \
        --ignore-platform-reqs \
        --no-ansi \
        --no-interaction \
        --no-scripts \
        --no-suggest \
        --no-progress \
        --prefer-dist
fi

if [ "${1}" = "test" ]; then
    php vendor/bin/phpunit
    exit 0
fi

if [ "${1}" = "migrate" ]; then
    wait-for-it "${DB_HOST}:${DB_PORT}" --timeout=60 -- php artisan migrate --force
    exit 0
fi

docker-php-entrypoint "$@"
