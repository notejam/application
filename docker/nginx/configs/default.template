server {
    listen 8080 default_server;
    listen [::]:8080;

    server_name _;

    root /app/public;

    # https redirect
    set $https_redirect ${HTTPS_REDIRECT};

    if ($request_uri = "/healthcheck") {
        set $https_redirect "off";
    }

    if ($http_x_forwarded_proto = "https") {
        set $https_redirect "off";
    }

    if ($https_redirect = "on") {
        return 301 https://$host$request_uri;
    }

    # security headers
    add_header X-Frame-Options "SAMEORIGIN" always;
    add_header X-XSS-Protection "1; mode=block" always;
    add_header X-Content-Type-Options "nosniff" always;
    add_header Referrer-Policy "no-referrer-when-downgrade" always;
    add_header Content-Security-Policy "default-src 'self' http: https: data: blob: 'unsafe-inline'" always;
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;

    # gzip
    gzip on;
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_types text/plain text/css text/xml application/json application/javascript application/rss+xml application/atom+xml image/svg+xml;

    index index.html index.htm index.php;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico {
        access_log off;
        log_not_found off;
    }

    location = /robots.txt  {
        access_log off;
        log_not_found off;
    }

    error_page 404 /index.php;

    location ~ \.php$ {
        include fastcgi_params;

        fastcgi_pass ${SERVICE_UPSTREAM}:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}

server {
    listen 8081 default_server;
    listen [::]:8081;

    server_name localhost;

    location /healthcheck {
        stub_status on;
        access_log   off;
    }
}
