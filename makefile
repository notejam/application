# if you see pwd_unknown showing up, this is why. Re-calibrate your system.
PWD ?= pwd_unknown

# PROJECT_NAME defaults to name of the current directory.
PROJECT_NAME = $(notdir $(PWD))

target := app
SERVICE_TARGET ?= $(target)

registry := registry.gitlab.com
REGISTRY_URL ?= $(registry)

CMD_ARGUMENTS ?= $(cmd)

ifeq ($(volumes),true)
	PRUNE_FLAGS := --volumes
endif

# Enable docker build kit
export DOCKER_BUILDKIT = 1
export COMPOSE_DOCKER_CLI_BUILD = 1

# all our targets are phony (no files to check).
.PHONY: exec help shell login init build up down restart logs test clean prune reset migrate

# suppress makes own output
.SILENT:

help:
	echo ''
	echo 'Usage: make [TARGET] [EXTRA_ARGUMENTS]'
	echo 'Targets:'
	echo '  init           initialize local application environment'
	echo '  build          build docker images'
	echo '  up             run docker containers'
	echo '  down           stop docker containers'
	echo '  restart        restart docker containers'
	echo '  test           run application tests'
	echo '  login          login in docker registry'
	echo '  logs           show application logs'
	echo '  migrate        migrates the database schema to the latest version'
	echo '  clean          stop and remove docker images'
	echo '  prune          cleanup all inactive containers and cache'
	echo '  reset          clean + prune and remove .env and vendor dir'
	echo '  shell          run docker single container'
	echo '  exec           exec command inside a docker container'
	echo ''
	echo 'Extra arguments:'
	echo '  cmd            make shell cmd="whoami"'
	echo '  registy        make login registry="custom.registry.com"'
	echo '  target         make exec target="webserver"'
	echo '  volumes        make prune volumes="true"'

login:
	docker login "$(REGISTRY_URL)"

init:
	test -f $(PWD)/.env || cp $(PWD)/.env.example $(PWD)/.env;

exec:
ifeq ($(CMD_ARGUMENTS),)
	# no command is given, default to shell
	docker-compose exec "$(SERVICE_TARGET)" sh
else
	# run the command
	docker-compose exec "$(SERVICE_TARGET)" sh -c "$(CMD_ARGUMENTS)"
endif

shell:
ifeq ($(CMD_ARGUMENTS),)
	# no command is given, default to shell
	docker-compose run --rm "$(SERVICE_TARGET)" sh
else
	# run the command
	docker-compose run --rm "$(SERVICE_TARGET)" sh -c "$(CMD_ARGUMENTS)"
endif

build:
	docker-compose build --no-cache

up:
	docker-compose up -d

down:
	docker-compose down

restart: down up

logs:
	docker-compose logs -f

migrate:
	docker-compose run --rm migration

test:
	# mock values for test docker-compose
	export CI_REGISTRY_IMAGE="$(REGISTRY_URL)"; \
	export CI_COMMIT_SHORT_SHA=$$(git rev-parse HEAD | cut -c 1-8); \
	docker-compose -p "$(PROJECT_NAME)-test" -f docker-compose.test.yml up --build --remove-orphans --abort-on-container-exit --exit-code-from "$(SERVICE_TARGET)"

clean:
	docker-compose down --remove-orphans --rmi all 2>/dev/null \
	&& echo 'Images for "$(PROJECT_NAME)" removed' \
	|| echo 'Images for "$(PROJECT_NAME)" already removed'

prune:
	docker system prune -af $(PRUNE_FLAGS)

reset: clean prune
	rm -rf .env src/vendor
