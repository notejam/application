# Notejam

![pipeline](https://gitlab.com/notejam/application/badges/master/pipeline.svg)

A proof of concept for continuous integration, testing, and deployment using [Notejam](https://github.com/komarserjio/notejam) application.

This repository contains the following:

- Docker implementation of Notejam and MySQL.
- CI/CD for test, build, deploy and scale on Kubernetes.
- A fork of Notejam in PHP/Laravel refactored into an n-tier application.

You can find the repository to create the infrastructure in terraform here: [IaC Infrastructure](https://gitlab.com/notejam/infrastructure)

## Requirements

Notejam requires the following tools:

- [Docker](https://docs.docker.com) >= 19.03
- [Docker Compose](https://docs.docker.com/compose) >= 1.25
- [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl) >= v1.15

## How to run the Notejam in development environment

This section describes how to configure and launch Notejam during development operations.

### Project configuration

Notejam requires environment variables to be present in `.env` file. The following environment variables must be specified:

| Environment Variable | Purpose |
| --- | --- |
| `APP_ENV` | Current application environment, can be: `local` or `production` |
| `DB_HOST` | DNS hostname or IP address of the MySQL server |
| `DB_PORT` | Database port for the MySQL server |
| `DB_NAME` | Database name for the MySQL server |
| `DB_USER` | User for the MySQL server |
| `DB_PASSWORD` | User password for the MySQL server |

Initialize the local application environment automatically:

```bash
make init up
```

The application will be available at: [http://localhost](http://localhost)

### CI/CD configuration

The CI pipelines requires the following secret enviroment variables in `CI/CD settings`:

| Variable | Purpose |
| --- | --- |
| `GOOGLE_KEY` | Google service account to use for GKE access |
| `SECRET_YAML` | Kubernetes secret manifests base64 encoded |

On the first time running you need to manually create the namespace in the cluster:

```bash
kubectl apply -f kube/namespace.yml
```

To create the production service account you can use the following script:

```bash
wget https://gist.githubusercontent.com/gadiener/37d9a4a57bc182d2f045108c7657e004/raw/d4661f63db468c8b0627ca57d7eb8f6ce825e32e/create-deployer-iam.sh && chmod +x create-deployer-iam.sh

NAME=notejam-deployer PROJECT=<your google project> ./create-deployer-iam.sh
```

It creates a service account with `container.viewer` role. Then, you need to copy the json printed in output after the message "Generating json key:" and use it as value for the `GOOGLE_KEY` variable.

Copy the deployer example file `cp gke-deployer.yml.example gke-deployer.yml` and replace the line 56 (`name: notejam-deployer@<project>.iam.gserviceaccount.com`) with your service account name. Now apply the manifest on the cluster:

```bash
kubectl apply -f gke-deployer.yml
```

To create the production secret set the correct values in the `.env` file and run the command:

```bash
kubectl create secret generic notejam-env --from-env-file .env -n notejam --dry-run=client | base64
```

Then copy the output and paste it in the `SECRET_YAML` variable.

### Testing

Run unit tests locally:

```bash
make test
```

If you've the project up and running, you can speed up the test phase with the following command:

```bash
make exec cmd='vendor/bin/phpunit'
```

### Other project commands

You can run the help command to view a list of all available commands:

```bash
make
```

## Note

This is a proof of concept of an n-tier implementation of Notejam with MySQL. It is lacking in certain features:

- Monolith: The logic of Notejam have not been broken out into microservices.
- Not fully secure: The application uses an obsolete version of php and laravel, it may contain known vulnerabilities.
- Logging: The application does not split the logs between stdout and stderr.
- Non-atomic deploy: Plain Kubernetes manifest have been used, the use of systems like HELM3 can guarantee atomic deploy and rollback.
- Caching: The database for sessions and cache is used, it would be better to use a database in memory.
