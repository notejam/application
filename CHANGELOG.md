# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/notejam/application/compare/v1.0.0...v1.0.1) (2020-05-19)


### Bug Fixes

* add missing GOOGLE_PROJECT variable in ci/cd configuration ([cd8805d](https://gitlab.com/notejam/application/commit/cd8805df121b81ea4555c25e119f01ab967208b2))
* add missing ROLLOUT_RESOURCES variable in ci/cd configuration ([7ab1a5a](https://gitlab.com/notejam/application/commit/7ab1a5a1c42e7b7eba42d9bc3dd48019372e958b))
* change wrong container name in the deployment ([84bc2d6](https://gitlab.com/notejam/application/commit/84bc2d67de6d1e1b78fad0e177a68d0f83e2df97))

## 1.0.0 (2020-05-19)


### Features

* add healthcheck route ([e54d8f1](https://gitlab.com/notejam/application/commit/e54d8f1440fe4e77fa9980b6afdcd8dd6c185570))
* add https redirect ([ac68eb8](https://gitlab.com/notejam/application/commit/ac68eb84a8c0b00810dd29b20d48ccc6113676f4))
* add kubernetes manifests ([4219a39](https://gitlab.com/notejam/application/commit/4219a3984704bdb6c1c845df121619af7cfda320))
* add notejam with docker and mysql ([ffa8815](https://gitlab.com/notejam/application/commit/ffa881564a820f0e1f96f44212c69946be77872b))


### Bug Fixes

* add missing dockerignore ([f6721d7](https://gitlab.com/notejam/application/commit/f6721d7148529c1b2783fa05eb8eb64ce31aec7e))
* change letsencrypt email ([25627c8](https://gitlab.com/notejam/application/commit/25627c850c0d99f059a3835b2d0c88ffd48cb3d8))
* change wrong route name ([c0403da](https://gitlab.com/notejam/application/commit/c0403da9c63a48506948ab0401891ad01653baad))
* mixed content error ([f7ecaa8](https://gitlab.com/notejam/application/commit/f7ecaa8fd5962e7d9f213295924a23f0b7ae0639))
* move cache and session to database for poc ([ca5cca5](https://gitlab.com/notejam/application/commit/ca5cca5c012452e9deb4d2cb7da8313695b36eee))
* move logs to stdout ([df75470](https://gitlab.com/notejam/application/commit/df7547040c6974f2eb478ffdca34ba61b095561b))
* remove default backend ([7d02fa5](https://gitlab.com/notejam/application/commit/7d02fa51dc56c236a885b03136c245dfdb9eb88c))
* remove unnecessary cidr from network policy ([5293894](https://gitlab.com/notejam/application/commit/52938941935b8c049a537597528750e74c6fdd00))
* remove unnecessary memory hpa ([004f0fa](https://gitlab.com/notejam/application/commit/004f0faf75b5613a9d560e8279e77d9fc9e373e7))
* server https for php-fpm ([331c4e3](https://gitlab.com/notejam/application/commit/331c4e3161e6b15aabcda50dfc0ef32c704c365d))
